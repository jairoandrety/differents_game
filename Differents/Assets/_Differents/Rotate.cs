﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    private enum Vectors
    { x,y,z }

     [SerializeField]
    private Vectors vectors;

    [SerializeField]
    private float speed = 0;

	private void Update ()
    {
        switch (vectors)
        {
            case Vectors.x:
                transform.Rotate(Vector3.right * speed * Time.deltaTime);
                break;
            case Vectors.y:
                transform.Rotate(Vector3.up * speed * Time.deltaTime);
                break;
            case Vectors.z:
                transform.Rotate(Vector3.forward * speed * Time.deltaTime);
                break;
        }

    }
}
