﻿using System;
using UnityEngine;

namespace Differents.Test
{
    public class Element : MonoBehaviour
    {
        public event Action<Element> OnPress;

        private bool check = false;

        void OnMouseDown()
        {
            if (check == true)
                return;

            check = true;
            OnPress.Invoke(this);
        }
    }


}

