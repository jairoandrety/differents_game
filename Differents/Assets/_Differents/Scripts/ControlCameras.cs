﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

using UnityEngine.EventSystems;

public class ControlCameras : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerEnterHandler, IPointerClickHandler, IPointerDownHandler, IPointerExitHandler
{
    [SerializeField] private GameObject PivotCenter;
    [SerializeField] private GameObject PivotCamera;

    [SerializeField] private List<GameObject> cameras;

    [SerializeField] private float speed;
    [SerializeField] private Vector3 direction;
    
    [SerializeField] Slider sliderDistance;

    [Header("Swiper")]
    public Vector3 pos = Vector3.zero;
    public Vector3 currentPos = Vector3.zero;
    public Vector3 lastPos = Vector3.zero;

    public Vector3 resultsPos = Vector3.zero;

    private void Awake()
    {
        sliderDistance.onValueChanged.AddListener(ChangeDistanceCamera);
    }

    private void Update()
    {
        PivotCenter.transform.Rotate(new Vector3(0, Mathf.Clamp(direction.y, -10f, 10f), 0) * speed * Time.deltaTime);

        float newHeith = PivotCamera.transform.eulerAngles.x + direction.x;
        PivotCamera.transform.localRotation = Quaternion.Euler(Mathf.Clamp(newHeith, 20f, 45f), 0, 0);
    }

    private void ChangeDistanceCamera(float distance)
    {
        cameras.ForEach(i => i.transform.localPosition = new Vector3(0, 0, distance));
    }

    public void OnPointerEnter(PointerEventData eventData)
    {       
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        direction = Vector3.zero;
    }

    public void OnPointerDown(PointerEventData eventData)
    {        
    }

    public void OnPointerClick(PointerEventData eventData)
    {        
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        pos = (Input.mousePosition);
    }

    public void OnDrag(PointerEventData eventData)
    {
        lastPos = (Input.mousePosition);

        ChecKActions();
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        direction = Vector3.zero;
        resultsPos = Vector3.zero;
    }

    void ChecKActions()
    {
        resultsPos.x = lastPos.x - pos.x;
        resultsPos.y = lastPos.y - pos.y;

        if (resultsPos.x != 0)
        {
            if (resultsPos.x > 50f)
                direction.y++;

            if (resultsPos.x < -50f)
                direction.y--;
        }

        if (resultsPos.y != 0)
        {
            if (resultsPos.y > 50f)
                direction.x++;

            if (resultsPos.y < -50f)
                direction.x--;
        }
    }

    private void OnDestroy()
    {
        sliderDistance.onValueChanged.RemoveAllListeners();
    }
}
