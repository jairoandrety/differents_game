﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlGame : MonoBehaviour
{

    [SerializeField] private int totalElements;
    [SerializeField] private int checkElements;

    public void CheckElements()
    {
        checkElements++;

        if (checkElements < totalElements)
        {
            Debug.Log("Continue Playing");
        }
        else
        {
            Debug.Log("Win Game");
        }
    }
}
