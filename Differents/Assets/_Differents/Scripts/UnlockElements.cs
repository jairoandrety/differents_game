﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Differents.Test
{
    public class UnlockElements : MonoBehaviour
    {
        private ControlGame controlGame;

        [SerializeField] private List<GameObject> parentsElements;
        [SerializeField] private List<GameObject> availableElements;
        [SerializeField] private List<Element> Elements;

        private void Awake()
        {
            if (controlGame == null)
                controlGame = FindObjectOfType<ControlGame>();
        }

        private void Start()
        {
            SetElements();
        }

        private void SetElements()
        {
            for (int i = 0; i < parentsElements.Count; i++)
            {
                for (int j = 0; j < parentsElements[i].transform.childCount; j++)
                {
                    availableElements.Add(parentsElements[i].transform.GetChild(j).gameObject);
                }
            }

            AddElements();
        }

        private void AddElements()
        {
            int select = UnityEngine.Random.Range(0, availableElements.Count);
            GameObject newElement = availableElements[select];

            bool exist = Elements.Exists(e => e == newElement);

            if (exist == true)
            {
                AddElements();
            }
            else
            {
                newElement.layer = 9;

                if (newElement.transform.childCount > 0)
                {
                    Transform[] hijos = newElement.GetComponentsInChildren<Transform>();
                    Array.ForEach(hijos, i => i.gameObject.layer = 9);
                }

                if (newElement.GetComponent<Element>() == null)
                    newElement.AddComponent<Element>();

                if (newElement.GetComponent<Element>() != null)
                {
                    Element elementHide = newElement.GetComponent<Element>();

                    elementHide.OnPress += Unlock;

                    Elements.Add(elementHide);
                    if (Elements.Count < 5)
                        AddElements();
                }
            }                
        }

        private void Unlock(Element element)
        {
            element.gameObject.layer = 0;
            if (element.transform.childCount > 0)
            {
                Transform[] hijos = element.GetComponentsInChildren<Transform>();
                Array.ForEach(hijos, i => i.gameObject.layer = 0);
            }

            element.OnPress -= Unlock;

            controlGame.CheckElements();
        }        
    }
}

