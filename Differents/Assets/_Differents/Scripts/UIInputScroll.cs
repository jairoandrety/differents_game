﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using UnityEngine.EventSystems;

namespace Logisticapp.UI
{
    public class UIInputScroll : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerEnterHandler, IPointerClickHandler, IPointerDownHandler
    {
        public Image derecha;
        public Image izquierda;

        [Header("Swiper")]
        public Vector3 pos = Vector3.zero;
        public Vector3 currentPos = Vector3.zero;
        public Vector3 lastPos = Vector3.zero;

        private bool activeButtons = true;

        public void OnPointerEnter(PointerEventData eventData)
        {
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            //if (derecha != null)
            //{
            //    if (eventData.pointerEnter == derecha.gameObject)
            //    {
            //        uiScroll.Derecha();
            //    }
            //}

            //if (izquierda != null)
            //{
            //    if (eventData.pointerEnter == izquierda.gameObject)
            //    {
            //        uiScroll.Izquierda();
            //    }
            //}
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (activeButtons == false)
                return;

            if (derecha != null)
            {
                if (eventData.pointerEnter == derecha.gameObject)
                {
                    
                }
            }

            if (izquierda != null)
            {
                if (eventData.pointerEnter == izquierda.gameObject)
                {
                    
                }
            }
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            pos = (Input.mousePosition);

            activeButtons = false;
        }

        public void OnDrag(PointerEventData eventData)
        {
            lastPos = (Input.mousePosition);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            ChecKActions();

            activeButtons = true;
        }

        void ChecKActions()
        {
            float resultX = 0;

            resultX = lastPos.x - pos.x;

            if (resultX < -50)
            {

            }

            if (resultX > 50)
            {
            }
        }
    }
}